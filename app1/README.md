# How it Works

## prerequisite 
 - Ensure you have Docker installed
 - Ensure you have kubernnetes installed
 - Ensure you have Git installed

## Pull Docker image from Docker repo
 - `docker pull himu1994/docker-php-helloworld:latest`

```
cd app1
kubectl apply -f app1-deployment.yaml
kubectl apply -f app1-service.yaml
```
### What You Should See
- - -
Go to your web browser and type
https://nodeip:30000

## Now You can See:

Hello I am APP1